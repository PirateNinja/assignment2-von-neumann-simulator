# Assignment2 - Von Neumann Simulator

In this assignment you have to build a Von Neumann simulator.

The Von Neumann architecture, also known as the Von Neumann model and Princeton architecture, is a computer architecture based on that described in 1945 by the mathematician and physicist John von Neumann and others in the First Draft of a Report on the EDVAC. This describes a design architecture for an electronic digital computer with parts consisting of a processing unit containing an arithmetic logic unit and processor registers, a control unit containing an instruction register and program counter, a memory to store both data and instructions, external mass storage, and input and output mechanisms. The meaning has evolved to be any stored-program computer in which an instruction fetch and a data operation cannot occur at the same time because they share a common bus. This is referred to as the Von Neumann bottleneck and often limits the performance of the system.

More info at https://en.wikipedia.org/wiki/Assembly_language

Objectives for this assignment are *events*. Events you will show by using the keys, clicks and interactions with the Von Neumann Simulator.

## Checklist
A spreadsheet where you can verify that all requirements are met is at https://goo.gl/4toUOh



## Components in the simulator
Components of the machine in the simulator.

All the components below are required.
```
        components: [
            {name: "PC", val: 0, label: "Program Counter"},
            {name: "MAR", val: null, label: "Memory Address Register"},
            {name: "RAM", val: null, label: "Memory (RAM)"},
            {name: "MBR", val: null, label: "Memory Buffer Register"},
            {name: "IR", val: null, label: "Instruction Register"},
            {name: "ALU", val: null, label: "ALU"},
            {name: "CU", val: null, label: "Control Unit"},
            {name: "INC", val: null, label: "Incrementer"},
            {name: "R", val: null, label: "Register"},
            {name: "PC2MAR", type: "bus"}
        ]
```


## Instructions in the assembly language

```
        instructions: [
            {instr: "load"},         // required
            {instr: "store"},        // required
            {instr: "copy"},         // optional
            {instr: "move"},         // required
            {instr: "add"},          // required
            {instr: "sub"},          // required
            {instr: "and"},          // required
            {instr: "or"},           // required
            {instr: "lshift"},       // optional
            {instr: "rshift"},       // optional
            {instr: "blt"},          // master
            {instr: "ble"},          // master
            {instr: "bgt"},          // master
            {instr: "bge"},          // master
            {instr: "beq"},          // master
            {instr: "bne"},          // master
            {instr: "jump"},         // optional
            {instr: "halt"}          // required
        ]
```

More info about assembly at https://en.wikipedia.org/wiki/Assembly_language


## Resources

**Example layout of components**
![https://bytebucket.org/theotheu/assignment2-von-neumann-simulator/raw/5d44176c661e2320116ad61500a449e0494d47d6/assets/Nuemann%20Simulator.png](https://bytebucket.org/theotheu/assignment2-von-neumann-simulator/raw/5d44176c661e2320116ad61500a449e0494d47d6/assets/Nuemann%20Simulator.png)

**Explanation about the machine**
https://www.youtube.com/watch?feature=player_detailpage&v=NdiJ-eAtFRc#t=174

**Function to transform decimal numbers to bits.**
```
dec2bin: function (dec, exp) {

    var i, iVal, bitStr;
    i = Math.pow(2, exp) - 1,
        bitStr = ""
    ;

    while (i >= 0) {
        iVal = Math.pow(2, i);
        if (dec - iVal >= 0) {
            bitStr += 1;
            dec -= iVal;
        } else {
            bitStr += 0;
        }
        i--;
    }

    return bitStr;
}
```

### Tips

**Keep separation between logical, physical components and the graphical display of the components**



**Structure of your program could be like**

- buildMachine
- readProgram
  - loadSampleProgram
- runProgram
  - fetchDecodeExecute
    - step
- done
